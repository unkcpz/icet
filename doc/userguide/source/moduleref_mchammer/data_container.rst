.. _data_container:

.. index::
   single: Function reference; DataContainer
   single: Class reference; DataContainer
   single: Monte Carlo; Data container

Data container
==============

.. autoclass:: mchammer.DataContainer
   :members:
   :undoc-members:
   :inherited-members:


.. _data_container_supporting_functions:

Supporting functions
--------------------

.. automodule:: mchammer.data_analysis
   :members:
   :undoc-members:
   :inherited-members:

