.. _cppref:
.. index::
   single: Function reference; C++ components
   single: Class reference; C++ components

Core components
***************

.. toctree::
   :maxdepth: 2

   additional_python_components
   python_interface_via_pybind
   cpp_classes
